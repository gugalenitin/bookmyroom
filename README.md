# Book My Room #
## 1.0 ##

Book My Room is a simple Ruby on Rails and Angular app to check the hotel room availability and book a room.


## Requirements ##

* RVM - https://rvm.io/rvm/install
* Ruby - ```rvm install ruby-2.4.0```
* RubyGems - ```rvm rubygems current```
* Ruby on Rails - ```gem install rails```
* MySQL - https://dev.mysql.com/downloads/installer/
* Node.js - https://nodejs.org/en/download/

## MySQL Setup ##

Create user and grant permissions -

```
#!sql

CREATE DATABASE bookmyroom_dev;
CREATE DATABASE bookmyroom_test;
CREATE DATABASE bookmyroom;

CREATE USER 'bookmyroom'@'localhost' IDENTIFIED BY 'bookmyroom@123';

GRANT ALL ON bookmyroom_dev.* TO 'bookmyroom'@'localhost';
GRANT ALL ON bookmyroom_test.* TO 'bookmyroom'@'localhost';
GRANT ALL ON bookmyroom.* TO 'bookmyroom'@'localhost';

SHOW GRANTS FOR 'bookmyroom'@'localhost';
```

## How to run? ##

**Test API -**

```
#!bash

cd api/

bundle install

bundle exec rspec
```

**Run API -**

```
#!bash

cd api/

bundle install

rails db:migrate

rails db:seed

rails s
```  


**Build Frontend -**
  
```
#!bash

cd frontend/

npm install

ng build
```
  

**Run Frontend -**
 
```
#!bash

cd frontend/

npm install

ng serve -o

```
