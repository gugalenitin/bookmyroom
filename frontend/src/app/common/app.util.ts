export class AppUtil {
  public static formatDate(date): string {
    if (date) {
      return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    }
  }

  public static formatIsAvailable(isAvailable): string {
    return isAvailable ? 'Yes' : 'No';
  }

}
