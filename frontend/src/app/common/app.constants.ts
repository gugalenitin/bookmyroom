export class AppConstants {
  public static baseURL(): string {
    return 'http://localhost:3000/';
  }

  public static hotelsAPI(): string {
    return AppConstants.baseURL() + 'hotels/';
  }

  public static roomTypesAPI(hotel_id): string {
    return AppConstants.hotelsAPI() + hotel_id + '/room_types/';
  }

  public static roomTypeGetAPI(bookingInfo): string {
    return AppConstants.roomTypesAPI(bookingInfo.hotel_id) + bookingInfo.room_type_id;
  }

  public static roomTypeAvailabilityAPI(bookingInfo): string {
    return AppConstants.roomTypeGetAPI(bookingInfo) + '/availability/?from_date='
      + bookingInfo.from_date + '&to_date=' + bookingInfo.to_date;
  }

  public static bookingsAPI(bookingInfo): string {
    return AppConstants.roomTypeGetAPI(bookingInfo) + '/bookings/';
  }
}
