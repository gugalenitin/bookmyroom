import { Component, OnInit } from '@angular/core';
import { HotelsDataService } from '../services/hotels.data.service';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';

import { AppUtil } from '../common/app.util';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  hotels: Object[];

  constructor(private hotelsDataService: HotelsDataService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.getHotels();
  }

  openHotel(hotel): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '520px',
      // @ts-ignore
      data: hotel
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getHotels();
    });
  }

  formatIsAvailable(isAvailable): string {
    return AppUtil.formatIsAvailable(isAvailable);
  }

  private getHotels(): void {
    this.hotelsDataService.getHotels().subscribe(data => {
      // @ts-ignore
      this.hotels = data;
    });
  }

}
