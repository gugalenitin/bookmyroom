import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppConstants } from '../common/app.constants'

@Injectable({
  providedIn: 'root'
})
export class RoomTypesDataService {

  constructor(private http: HttpClient) { }

  getRoomTypes(hotel) {
    return this.http.get(AppConstants.roomTypesAPI(hotel.slug));
  }

  getAvailability(roomType) {
    return this.http.get(AppConstants.roomTypeAvailabilityAPI(roomType));
  }
}
