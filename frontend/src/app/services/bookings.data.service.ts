import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppConstants } from '../common/app.constants'

@Injectable({
  providedIn: 'root'
})
export class BookingsDataService {

  constructor(private http: HttpClient) { }

  createBooking(booking) {
    return this.http.post(AppConstants.bookingsAPI(booking), booking);
  }
}
