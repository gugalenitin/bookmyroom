import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppConstants } from '../common/app.constants'

@Injectable({
  providedIn: 'root'
})
export class HotelsDataService {

  constructor(private http: HttpClient) { }

  getHotels() {
    return this.http.get(AppConstants.hotelsAPI());
  }
}
