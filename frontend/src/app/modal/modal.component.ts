import { Component, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { RoomTypesDataService } from '../services/room-types.data.service';
import { BookingsDataService } from '../services/bookings.data.service';
import { AppUtil } from '../common/app.util';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {

  roomTypes: Object[];
  roomTypeAvailability: Object = {
    rent_per_month: 0,
    is_available: false
  };
  roomType: FormControl = new FormControl();
  fromDate: FormControl = new FormControl();
  toDate: FormControl = new FormControl();
  customerName: FormControl = new FormControl('', [
    Validators.required,
  ]);
  bookingStatus: string = 'Pending';
  error: string;

  constructor(
    private roomTypesDataService: RoomTypesDataService,
    private bookingsDataService: BookingsDataService,
    private dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) private hotel: Object) {

    this.roomTypesDataService.getRoomTypes(hotel).subscribe(data => {
      // @ts-ignore
      this.roomTypes = data;
    });
  }

  getAvailability(): void {
    if (this.canGetAvailability()) {
      this.error = '';
      this.roomTypesDataService.getAvailability(this.createBookingInfo()).subscribe(data => {
        // @ts-ignore
        if (data.room_type_id) {
          this.roomTypeAvailability = data;
        } else {
          // @ts-ignore
          this.error = data.message;
        }
      }, error => {
        this.error = error.statusText;
      });
    }
  }

  onBook(): void {
    if (this.canBook()) {
      this.error = '';
      this.bookingsDataService.createBooking(this.createBookingInfo()).subscribe(data => {
        // @ts-ignore
        if (data.id) {
          this.bookingStatus = 'Successful';
          this.getAvailability();
        } else {
          this.bookingStatus = 'Failed';
          // @ts-ignore
          this.error = data.message;
        }
      }, error => {
        this.error = error.statusText;
        this.bookingStatus = 'Failed';
      });
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  formatIsAvailable(isAvailable): string {
    return AppUtil.formatIsAvailable(isAvailable);
  }

  canBook() : boolean {
    return this.showCustomerName() && this.customerName.value;
  }

  showCustomerName() : boolean {
    // @ts-ignore
    return this.canGetAvailability() && this.roomTypeAvailability.is_available;
  }

  canGetAvailability() : boolean {
    return this.roomType.value && this.fromDate.value && this.toDate.value;
  }

  private createBookingInfo(): Object {
    return {
      // @ts-ignore
      hotel_id: this.hotel.slug,
      room_type_id: this.roomType.value ? this.roomType.value.slug : null,
      from_date: AppUtil.formatDate(this.fromDate.value),
      to_date: AppUtil.formatDate(this.toDate.value),
      customer_name: this.customerName.value
    };
  }

}
