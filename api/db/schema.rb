# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_21_212437) do

  create_table "bookings", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.bigint "room_type_id"
    t.date "from_date"
    t.date "to_date"
    t.decimal "amount", precision: 10
    t.string "customer_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["room_type_id"], name: "index_bookings_on_room_type_id"
  end

  create_table "hotels", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name"
    t.string "cover_image"
    t.boolean "is_available"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.index ["slug"], name: "index_hotels_on_slug", unique: true
  end

  create_table "rents", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.bigint "room_type_id"
    t.date "date"
    t.decimal "amount", precision: 10
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_booked", default: false
    t.index ["room_type_id", "date"], name: "index_rents_on_room_type_id_and_date", unique: true
    t.index ["room_type_id"], name: "index_rents_on_room_type_id"
  end

  create_table "room_types", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name"
    t.string "cover_image"
    t.bigint "hotel_id"
    t.integer "capacity"
    t.boolean "is_available"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.index ["hotel_id"], name: "index_room_types_on_hotel_id"
    t.index ["slug"], name: "index_room_types_on_slug", unique: true
  end

  add_foreign_key "bookings", "room_types"
  add_foreign_key "rents", "room_types"
  add_foreign_key "room_types", "hotels"
end
