class AddIsBookedToRents < ActiveRecord::Migration[5.2]
  def change
    add_column :rents, :is_booked, :boolean, default: false
  end
end
