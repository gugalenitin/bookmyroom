class CreateHotels < ActiveRecord::Migration[5.2]
  def change
    create_table :hotels do |t|
      t.string :name
      t.string :cover_image
      t.boolean :is_available

      t.timestamps
    end
  end
end
