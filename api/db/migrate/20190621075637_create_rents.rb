class CreateRents < ActiveRecord::Migration[5.2]
  def change
    create_table :rents do |t|
      t.references :room_type, foreign_key: true
      t.date :date
      t.decimal :amount

      t.timestamps
    end
  end
end
