class CreateBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
      t.references :room_type, foreign_key: true
      t.date :from_date
      t.date :to_date
      t.decimal :amount
      t.string :customer_name

      t.timestamps
    end
  end
end
