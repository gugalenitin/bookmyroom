class CreateRoomTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :room_types do |t|
      t.string :name
      t.string :cover_image
      t.references :hotel, foreign_key: true
      t.integer :capacity
      t.boolean :is_available

      t.timestamps
    end
  end
end
