class AddRoomTypeIdDateIndexToRents < ActiveRecord::Migration[5.2]
  def change
    add_index :rents, [:room_type_id, :date], unique: true
  end
end
