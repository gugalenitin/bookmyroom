# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Clean database
Booking.delete_all
Rent.delete_all
RoomType.delete_all
Hotel.delete_all

# Create hotels
Hotel.create!(name: 'Hyatt', cover_image: 'https://fakeimg.pl/60x60/')
Hotel.create!(name: 'JW Marriot', cover_image: 'https://fakeimg.pl/60x60/')
Hotel.create!(name: 'The Taj', cover_image: 'https://fakeimg.pl/60x60/')
Hotel.create!(name: 'Novotel', cover_image: 'https://fakeimg.pl/60x60/')
Hotel.create!(name: 'Cloud Green', cover_image: 'https://fakeimg.pl/60x60/')

# Create room_types
hotels = Hotel.all
RoomType.create!(name: 'Standard', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[0], capacity: 2)
RoomType.create!(name: 'Standard', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[1], capacity: 2)
RoomType.create!(name: 'Standard', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[2], capacity: 2)
RoomType.create!(name: 'Standard', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[3], capacity: 2)
RoomType.create!(name: 'Standard', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[4], capacity: 2)
RoomType.create!(name: 'Standard with AC', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[0], capacity: 2)
RoomType.create!(name: 'Standard with AC', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[1], capacity: 2)
RoomType.create!(name: 'Standard with AC', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[2], capacity: 2)
RoomType.create!(name: 'Delux AC', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[3], capacity: 3)
RoomType.create!(name: 'Delux AC', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[4], capacity: 3)
RoomType.create!(name: 'Family Non AC', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[0], capacity: 4)
RoomType.create!(name: 'Family Non AC', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[4], capacity: 4)
RoomType.create!(name: 'Suite AC', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[2], capacity: 6)
RoomType.create!(name: 'Suite AC', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[3], capacity: 6)
RoomType.create!(name: 'Suite AC', cover_image: 'https://fakeimg.pl/120x120/', hotel: hotels[4], capacity: 6)

# Create rents
room_types = RoomType.all
Rent.create!(date: '2019-07-01', amount: 200.0, room_type: room_types[0])
Rent.create!(date: '2019-07-02', amount: 300.0, room_type: room_types[0])
Rent.create!(date: '2019-07-03', amount: 250.0, room_type: room_types[0])
Rent.create!(date: '2019-07-04', amount: 250.0, room_type: room_types[0])
Rent.create!(date: '2019-07-05', amount: 150.0, room_type: room_types[0])
Rent.create!(date: '2019-07-01', amount: 250.0, room_type: room_types[11])
Rent.create!(date: '2019-07-01', amount: 300.0, room_type: room_types[2])
Rent.create!(date: '2019-07-01', amount: 200.0, room_type: room_types[4])
Rent.create!(date: '2019-07-02', amount: 300.0, room_type: room_types[5])
Rent.create!(date: '2019-07-04', amount: 900.0, room_type: room_types[7])
Rent.create!(date: '2019-07-02', amount: 300.0, room_type: room_types[8])
Rent.create!(date: '2019-07-03', amount: 300.0, room_type: room_types[5])
Rent.create!(date: '2019-07-04', amount: 230.0, room_type: room_types[4])
Rent.create!(date: '2019-07-05', amount: 300.0, room_type: room_types[13])
Rent.create!(date: '2019-07-03', amount: 430.0, room_type: room_types[7])
Rent.create!(date: '2019-07-01', amount: 300.0, room_type: room_types[9])
Rent.create!(date: '2019-07-02', amount: 300.0, room_type: room_types[10])
Rent.create!(date: '2019-07-04', amount: 200.0, room_type: room_types[12])
Rent.create!(date: '2019-07-05', amount: 200.0, room_type: room_types[12])
Rent.create!(date: '2019-07-05', amount: 400.0, room_type: room_types[9])
Rent.create!(date: '2019-07-03', amount: 700.0, room_type: room_types[3])
Rent.create!(date: '2019-07-02', amount: 800.0, room_type: room_types[4])
Rent.create!(date: '2019-07-03', amount: 300.0, room_type: room_types[11])
Rent.create!(date: '2019-07-02', amount: 500.0, room_type: room_types[7])
Rent.create!(date: '2019-07-04', amount: 900.0, room_type: room_types[9])
Rent.create!(date: '2019-07-05', amount: 200.0, room_type: room_types[2])
Rent.create!(date: '2019-07-03', amount: 1000.0, room_type: room_types[10])
Rent.create!(date: '2019-07-02', amount: 2000.0, room_type: room_types[11])