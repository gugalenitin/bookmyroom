class Hotel < ApplicationRecord
  extend FriendlyId

  friendly_id :name, use: :slugged

  # Model association
  has_many :room_types, dependent: :destroy

  # Validations
  validates_presence_of :name
end
