class Rent < ApplicationRecord
  # Model association
  belongs_to :room_type

  # Validations
  validates_presence_of :date, :amount
end
