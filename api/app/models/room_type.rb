class RoomType < ApplicationRecord
  extend FriendlyId

  friendly_id :name, use: :slugged

  # Model association
  belongs_to :hotel
  has_many :rents, dependent: :destroy
  has_many :bookings, dependent: :destroy

  # Validation
  validates_presence_of :name, :capacity
end
