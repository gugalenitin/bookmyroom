class Booking < ApplicationRecord
  # Model association
  belongs_to :room_type

  # Validations
  validates_presence_of :from_date, :to_date, :amount, :customer_name
end
