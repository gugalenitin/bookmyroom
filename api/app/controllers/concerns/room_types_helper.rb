module RoomTypesHelper
  MONTH_DAYS = 30

  def is_available_between?(room_type, from_date, to_date)
    from_date = Date.parse(from_date)
    to_date = Date.parse(to_date)
    raise 'from_date should be older than or equal to to_date' if from_date > to_date

    rent_per_month, total_amount = 0, 0
    rents = rents_between(room_type, from_date, to_date)
    num_days = (to_date - from_date).to_i + 1
    # Rent details should be available for all the days
    is_available = rents.count == num_days

    # Calculate rent_per_month and total_amount only if the room_type is available
    if is_available
      # Total amount for the selected days
      total_amount = rents.sum(&:amount)
      # Rent is the average rent for the duration * 30 days
      # rent_per_month
      rent_per_month = total_amount / num_days * MONTH_DAYS
    end

    {
        room_type_id: room_type.id,
        from_date: from_date,
        to_date: to_date,
        is_available: is_available,
        rent_per_month: rent_per_month,
        total_amount: total_amount,
    }
  end

  def rents_between(room_type, from_date, to_date)
    room_type.rents
        .where('date >= ?', from_date)
        .where('date <= ?', to_date)
        .where(is_booked: false)
  end

end