class RentsController < ApplicationController

  before_action :set_room_type
  before_action :set_rent, only: [:show, :update, :destroy]

  # GET /hotel/:hotel_id/room_types/:room_type_id/rents
  def index
    json_response(@room_type.rents)
  end

  # POST /hotel/:hotel_id/room_types/:room_type_id/rents
  def create
    @rent = @room_type.rents.create!(rent_params)
    json_response(@rent, :created)
  end

  # GET /hotel/:hotel_id/room_types/:room_type_id/rents/:id
  def show
    json_response(@rent)
  end

  # PUT /hotel/:hotel_id/room_types/:room_type_id/rents/:id
  def update
    @rent.update(rent_params)
    head :no_content
  end

  # DELETE /hotel/:hotel_id/room_types/:room_type_id/rents/:id
  def destroy
    @rent.destroy
    head :no_content
  end

  private

  def rent_params
    # whitelist params
    params.permit(:date, :amount)
  end

  def set_room_type
    hotel = Hotel.friendly.find(params[:hotel_id])
    @room_type = hotel.room_types.friendly.find(params[:room_type_id]) if hotel
  end

  def set_rent
    @rent = @room_type.rents.find(params[:id]) if @room_type
  end
end
