class RoomTypesController < ApplicationController
  include RoomTypesHelper

  before_action :set_hotel
  before_action :set_room_type, only: [:show, :update, :destroy, :is_available?]

  # GET /hotel/:hotel_id/room_types
  def index
    room_types = @hotel.room_types
    update_availabilities(room_types)
    json_response(room_types)
  end

  # POST /hotel/:hotel_id/room_types
  def create
    @room_type = @hotel.room_types.create!(room_type_params)
    json_response(@room_type, :created)
  end

  # GET /hotel/:hotel_id/room_types/:id
  def show
    json_response(@room_type)
  end

  # PUT /hotel/:hotel_id/room_types/:id
  def update
    @room_type.update(room_type_params)
    head :no_content
  end

  # DELETE /hotel/:hotel_id/room_types/:id
  def destroy
    @room_type.destroy
    head :no_content
  end

  # GET /hotel/:hotel_id/room_types/:id/availability
  def is_available?
    params.require(:from_date)
    params.require(:to_date)
    json_response(is_available_between?(@room_type, params[:from_date], params[:to_date]))
  end

  private

  def room_type_params
    # whitelist params
    params.permit(:name, :capacity)
  end

  def set_hotel
    @hotel = Hotel.friendly.find(params[:hotel_id])
  end

  def set_room_type
    @room_type = @hotel.room_types.friendly.find(params[:id] ? params[:id] : params[:room_type_id]) if @hotel
    update_availability(@room_type)
  end

  def update_availability(room_type)
    room_type.is_available = room_type.rents.exists?(is_booked: false)
  end

  def update_availabilities(room_types)
    room_types.each { |room_type| update_availability(room_type) }
  end
end
