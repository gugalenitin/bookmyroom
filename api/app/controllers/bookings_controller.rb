class BookingsController < ApplicationController
  include RoomTypesHelper

  before_action :set_room_type
  before_action :set_booking, only: [:show]

  # POST /hotel/:hotel_id/room_types/:room_type_id/bookings
  def create
    params.require(:from_date)
    params.require(:to_date)
    avail_details = is_available_between?(@room_type, params[:from_date], params[:to_date])
    raise 'Room type is not available for the selected dates' unless avail_details[:is_available]

    booking = booking_params
    booking[:amount] = avail_details[:total_amount]
    @booking = @room_type.bookings.create!(booking)
    rents_between(@room_type, params[:from_date], params[:to_date]).update_all(is_booked: true)
    json_response(@booking, :created)
  end

  # GET /hotel/:hotel_id/room_types/:room_type_id/bookings/:id
  def show
    json_response(@booking)
  end

  private

  def booking_params
    # whitelist params
    params.permit(:from_date, :to_date, :customer_name)
  end

  def set_room_type
    hotel = Hotel.friendly.find(params[:hotel_id])
    @room_type = hotel.room_types.friendly.find(params[:room_type_id]) if hotel
  end

  def set_booking
    @booking = @room_type.bookings.find(params[:id]) if @room_type
  end
end
