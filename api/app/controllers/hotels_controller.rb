class HotelsController < ApplicationController

  before_action :set_hotel, only: [:show, :update, :destroy]

  # GET /hotels
  def index
    hotels = Hotel.all
    update_availabilities(hotels)
    json_response(hotels)
  end

  # POST /hotels
  def create
    @hotel = Hotel.create!(hotel_params)
    json_response(@hotel, :created)
  end

  # GET /hotels/:id
  def show
    json_response(@hotel)
  end

  # PUT /hotels/:id
  def update
    @hotel.update(hotel_params)
    head :no_content
  end

  # DELETE /hotels/:id
  def destroy
    @hotel.destroy
    head :no_content
  end

  private

  def hotel_params
    # whitelist params
    params.permit(:name)
  end

  def set_hotel
    @hotel = Hotel.friendly.find(params[:id])
    update_availability(@hotel)
  end

  def update_availability(hotel)
    hotel.room_types.each do |room_type|
      hotel.is_available = room_type.rents.exists?(is_booked: false)
      break if hotel.is_available
    end
  end

  def update_availabilities(hotels)
    hotels.each { |hotel| update_availability(hotel) }
  end
end
