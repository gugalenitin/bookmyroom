require 'rails_helper'

RSpec.describe 'RoomTypes API', type: :request do
  # initialize test data
  let!(:hotel) { create(:hotel) }
  let!(:room_types) { create_list(:room_type, 10, hotel_id: hotel.id) }
  let(:hotel_id) { hotel.id }
  let(:room_type_id) { room_types.first.id }

  # Test suite for GET /hotels/:hotel_id/room_types
  describe 'GET /hotels/:hotel_id/room_types' do
    # make HTTP get request before each example
    before { get "/hotels/#{ hotel_id }/room_types" }

    it 'returns room_types' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /hotels/:hotel_id/room_types/:id
  describe 'GET /hotels/:hotel_id/room_types/:id' do
    before { get "/hotels/#{ hotel_id }/room_types/#{ room_type_id }" }

    context 'when the record exists' do
      it 'returns the room_type' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(room_type_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:room_type_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find RoomType/)
      end
    end
  end

  # Test suite for POST /hotels/:hotel_id/room_types
  describe 'POST /hotels/:hotel_id/room_types' do
    # valid payload
    let(:valid_attributes) { { name: 'Standard', capacity: 2 } }

    context 'when the request is valid' do
      before { post "/hotels/#{ hotel_id }/room_types", params: valid_attributes }

      it 'creates a room_type' do
        expect(json['name']).to eq('Standard')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post "/hotels/#{ hotel_id }/room_types", params: { } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
            .to match(/Validation failed: Name can't be blank/)
      end
    end
  end

  # Test suite for PUT /hotels/:hotel_id/room_types/:id
  describe 'PUT /hotels/:hotel_id/room_types/:id' do
    let(:valid_attributes) { { name: 'Standard' } }

    context 'when the record exists' do
      before { put "/hotels/#{ hotel_id }/room_types/#{room_type_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # Test suite for DELETE /hotels/:hotel_id/room_types/:id
  describe 'DELETE /hotels/:hotel_id/room_types/:id' do
    before { delete "/hotels/#{ hotel_id }/room_types/#{room_type_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end

  # Test suite for GET /hotels/:hotel_id/room_types/:id/availability
  describe 'GET /hotels/:hotel_id/room_types/:id/availability' do
    before { get "/hotels/#{ hotel_id }/room_types/#{ room_type_id }/availability?from_date=2019-10-23&to_date=2019-10-25" }

    context 'when the record exists' do
      it 'returns the availability' do
        expect(json).not_to be_empty
        expect(json['is_available']).to eq(false)
        expect(json['rent_per_month']).to eq(0)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end
end