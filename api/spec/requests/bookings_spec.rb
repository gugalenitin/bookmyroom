require 'rails_helper'

RSpec.describe 'Bookings API', type: :request do
  # initialize test data
  let!(:hotel) { create(:hotel) }
  let!(:room_type) { create(:room_type, hotel_id: hotel.id) }
  let!(:rent) { create(:rent, room_type_id: room_type.id) }
  let!(:booking) { create(:booking, room_type_id: room_type.id) }
  let(:hotel_id) { hotel.id }
  let(:room_type_id) { room_type.id }
  let(:booking_id) { booking.id }

  # Test suite for GET /hotels/:hotel_id/room_types/:room_type_id/bookings/:id
  describe 'GET /hotels/:hotel_id/room_types/:room_type_id/bookings/:id' do
    before { get "/hotels/#{ hotel_id }/room_types/#{ room_type_id }/bookings/#{ booking_id }" }

    context 'when the record exists' do
      it 'returns the booking' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(booking_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:booking_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Booking/)
      end
    end
  end

  # Test suite for POST /hotels/:hotel_id/room_types/:room_type_id/bookings
  describe 'POST /hotels/:hotel_id/room_types/:room_type_id/bookings' do
    # valid payload
    let(:valid_attributes) { { from_date: booking.from_date, to_date: booking.to_date, amount: 200.00, customer_name: 'John' } }

    context 'when the request is valid' do
      before { post "/hotels/#{ hotel_id }/room_types/#{ room_type_id }/bookings", params: valid_attributes }

      it 'creates a booking' do
        expect(json['customer_name']).to eq('John')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post "/hotels/#{ hotel_id }/room_types/#{ room_type_id }/bookings", params: { } }

      it 'returns a params missing failure message' do
        expect(response.body)
            .to match(/param is missing or the value is empty: from_date/)
      end
    end
  end

end