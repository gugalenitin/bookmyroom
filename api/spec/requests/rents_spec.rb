require 'rails_helper'

RSpec.describe 'Rents API', type: :request do
  # initialize test data
  let!(:hotel) { create(:hotel) }
  let!(:room_type) { create(:room_type, hotel_id: hotel.id) }
  let!(:rents) { create_list(:rent, 1, room_type_id: room_type.id) }
  let(:hotel_id) { hotel.id }
  let(:room_type_id) { room_type.id }
  let(:rent_id) { rents.first.id }

  # Test suite for GET /hotels/:hotel_id/room_types/:room_type_id/rents
  describe 'GET /hotels/:hotel_id/room_types/:room_type_id/rents' do
    # make HTTP get request before each example
    before { get "/hotels/#{ hotel_id }/room_types/#{ room_type_id }/rents" }

    it 'returns rents' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(1)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /hotels/:hotel_id/room_types/:room_type_id/rents/:id
  describe 'GET /hotels/:hotel_id/room_types/:room_type_id/rents/:id' do
    before { get "/hotels/#{ hotel_id }/room_types/#{ room_type_id }/rents/#{ rent_id }" }

    context 'when the record exists' do
      it 'returns the rent' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(rent_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:rent_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Rent/)
      end
    end
  end

  # Test suite for POST /hotels/:hotel_id/room_types/:room_type_id/rents
  describe 'POST /hotels/:hotel_id/room_types/:room_type_id/rents' do
    # valid payload
    let(:valid_attributes) { { date: '2019-06-20', amount: 200.00 } }

    context 'when the request is valid' do
      before { post "/hotels/#{ hotel_id }/room_types/#{ room_type_id }/rents", params: valid_attributes }

      it 'creates a rent' do
        expect(json['amount']).to eq(200.00)
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post "/hotels/#{ hotel_id }/room_types/#{ room_type_id }/rents", params: { } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
            .to match(/Validation failed: Date can't be blank/)
      end
    end
  end

  # Test suite for PUT /hotels/:hotel_id/room_types/:room_type_id/rents/:id
  describe 'PUT /hotels/:hotel_id/room_types/:room_type_id/rents/:id' do
    let(:valid_attributes) { { amount: 300.00 } }

    context 'when the record exists' do
      before { put "/hotels/#{ hotel_id }/room_types/#{ room_type_id }/rents/#{rent_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # Test suite for DELETE /hotels/:hotel_id/room_types/:room_type_id/rents/:id
  describe 'DELETE /hotels/:hotel_id/room_types/:room_type_id/rents/:id' do
    before { delete "/hotels/#{ hotel_id }/room_types/#{ room_type_id }/rents/#{rent_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end