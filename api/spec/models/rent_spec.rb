require 'rails_helper'

RSpec.describe Rent, type: :model do
  # Association test
  # Ensure Rent record belongs to a single RoomType record
  it { should belong_to(:room_type) }

  # Validation test
  # Ensure columns date, amount are present before saving
  it { should validate_presence_of(:date) }
  it { should validate_presence_of(:amount) }
end
