require 'rails_helper'

RSpec.describe Hotel, type: :model do
  # Association test
  # Ensure Hotel model has a 1:m relationship with the RoomType model
  it { should have_many(:room_types).dependent(:destroy) }

  # Validation tests
  # Ensure column name is present before saving
  it { should validate_presence_of(:name) }
end
