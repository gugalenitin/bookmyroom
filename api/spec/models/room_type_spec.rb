require 'rails_helper'

RSpec.describe RoomType, type: :model do
  # Association test
  # Ensure RoomType record belongs to a single Hotel record
  it { should belong_to(:hotel) }
  # Ensure RoomType model has a 1:m relationship with the Rent, Booking models
  it { should have_many(:rents).dependent(:destroy) }
  it { should have_many(:bookings).dependent(:destroy) }

  # Validation test
  # Ensure columns name, capacity are present before saving
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:capacity) }
end
