require 'rails_helper'

RSpec.describe Booking, type: :model do
  # Association test
  # Ensure Booking record belongs to a single RoomType record
  it { should belong_to(:room_type) }

  # Validation test
  # Ensure columns from_date, to_date, amount are present before saving
  it { should validate_presence_of(:from_date) }
  it { should validate_presence_of(:to_date) }
  it { should validate_presence_of(:amount) }
end
