FactoryBot.define do
  factory :booking do
    from_date { Faker::Date.forward(1) }
    to_date { Faker::Date.forward(1) }
    amount { Faker::Number.decimal(2) }
    room_type_id { nil }
    customer_name { Faker::Name.name }
  end
end