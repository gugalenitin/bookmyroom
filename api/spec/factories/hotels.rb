FactoryBot.define do
  factory :hotel do
    name { Faker::Name.name }
    cover_image { Faker::Avatar.image }
    is_available { Faker::Boolean.boolean }
  end
end