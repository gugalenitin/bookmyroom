FactoryBot.define do
  factory :room_type do
    name { Faker::Name.name }
    cover_image { Faker::Avatar.image }
    capacity { Faker::Number.number(2) }
    is_available { Faker::Boolean.boolean }
    hotel_id { nil }
  end
end