FactoryBot.define do
  factory :rent do
    date { Faker::Date.forward(1) }
    amount { Faker::Number.decimal(2) }
    room_type_id { nil }
  end
end