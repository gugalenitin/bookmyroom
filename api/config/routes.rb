Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :hotels do
    resources :room_types do
      resources :rents
      resources :bookings, only: [:create, :show]

      get '/availability', to: 'room_types#is_available?'
    end
  end
end
